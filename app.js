require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3001
const cors = require('cors')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())

//routes
app.use(require('./routes/index'))

app.listen(port, () => {
    console.log(`Listening on port: ${port}`)
})