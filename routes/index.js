require('dotenv').config();
const router = require('express').Router();
const axios = require('axios')
const {google} = require('googleapis')

const youtube = google.youtube({
    version: 'v3',
    auth: process.env.YOUTUBE_KEY
})
const regularLink = "https://www.youtube.com/watch?v="
router.get('/search', async (req, res, next) => {
    console.log(req.query)
    const {q} = req.query
    try{
        const response = await youtube.search.list({
            part: 'snippet',
            q: q
        })
        const items = response.data.items
        let results = []
        items.forEach(element => {
            let obj = {}
            obj.title = element.snippet.title
            obj.description = element.snippet.description
            obj.id = regularLink+element.id.videoId
            obj.thumbnail = element.snippet.thumbnails.medium.url
            results.push(obj)
        });

        res.send(results)
        results = []

    }catch(e){
        next(e)
    }
  
})

module.exports = router